'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ItemImagesSchema extends Schema {
  up () {
    this.create('item_images', (table) => {
      table.increments()
      table.integer('item_id')
      table.string('url')
      table.string('thumbnail_url')
      table.string('caption')
      table.timestamps()
    })
  }

  down () {
    this.drop('item_images')
  }
}

module.exports = ItemImagesSchema
