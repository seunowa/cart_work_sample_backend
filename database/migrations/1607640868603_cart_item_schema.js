'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CartItemSchema extends Schema {
  up () {
    this.create('cart_items', (table) => {
      table.increments()
      table.string('username')
      table.integer('item_id')
      table.integer('quantity')
      table.timestamps()
    })
  }

  down () {
    this.drop('cart_items')
  }
}

module.exports = CartItemSchema
