'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CartItemSchema extends Schema {
  up () {
    this.create('carts', (table) => {
      table.increments()
      table.string('username')
      table.timestamps()
    })
  }

  down () {
    this.drop('carts')
  }
}

module.exports = CartItemSchema
