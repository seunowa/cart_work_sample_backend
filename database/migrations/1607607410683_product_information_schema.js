'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductInformationSchema extends Schema {
  up () {
    this.create('product_informations', (table) => {
      table.increments()
      table.integer('item_id')
      table.string('caption')
      table.string('value')
      table.timestamps()
    })
  }

  down () {
    this.drop('product_informations')
  }
}

module.exports = ProductInformationSchema
