'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ViewsSchema extends Schema {
  up () {
    this.create('views', (table) => {
      table.increments()
      table.string('username')
      table.integer('item_id')
      table.timestamps()
    })
  }

  down () {
    this.drop('views')
  }
}

module.exports = ViewsSchema
