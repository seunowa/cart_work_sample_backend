'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ItemCategorySchema extends Schema {
  up () {
    this.create('item_categories', (table) => {
      table.increments()
      table.integer('item_id')
      table.integer('item_category_id')
      table.timestamps()
    })
  }

  down () {
    this.drop('item_categories')
  }
}

module.exports = ItemCategorySchema
