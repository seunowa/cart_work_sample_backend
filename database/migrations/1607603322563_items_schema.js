'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ItemsSchema extends Schema {
  
  up () {
    this.create('items', (table) => {
      table.increments()
      table.string('caption')
      table.string('description')
      table.decimal('price')
      table.integer('qty_in_store')
      table.timestamps()
    })
  }

  down () {
    this.drop('items')
  }
}

module.exports = ItemsSchema
