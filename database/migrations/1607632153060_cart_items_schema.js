'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CartItemsSchema extends Schema {
  up () {
    this.create('carts_items', (table) => {
      table.increments()
      table.string('username')
      table.integer('item_id')
      table.integer('quantity')
      table.timestamps()
    })
  }

  down () {
    this.drop('carts_items')
  }
}

module.exports = CartItemsSchema
