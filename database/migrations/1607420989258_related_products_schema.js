'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RelatedProductsSchema extends Schema {
  up () {
    this.create('related_products', (table) => {
      table.increments()
      table.integer('item_id')
      table.integer('related_item_id')
      table.timestamps()
    })
  }

  down () {
    this.drop('related_products')
  }
}

module.exports = RelatedProductsSchema
