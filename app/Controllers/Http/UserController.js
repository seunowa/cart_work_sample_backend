'use strict'
const User = use('App/Models/User')

class UserController {
    //Strore User
    async store({request, response}){  
        
        const user = request.all()

        await User.create(user)
        //await item.save()
        return response.ok("User created")
    }

    async login({request, response, auth}){
        const input = request.all()

        try{
            const token = await auth
                    //.withRefreshToken()
                    .withRefreshToken()
                    .attempt(input.email, input.password)
            return response.ok(token)
        }catch(error){
            console.log(error)
        }

    }

    async fetchUser({response, auth}){
        try{
            const user = await auth.getUser()
            response.ok(user)
        }catch(error){
            console.log(error)
            //response.send('user not known')
        }
    }
}

module.exports = UserController
