'use strict'
//const Axios = use('Axios')
const Item = use('App/Models/Item')
const {validate} = use('Validator')
const ProductInformation = use('App/Models/ProductInformation')
const Comment = use('App/Models/Comment')
const Database = use('Database')

class ItemController {

    //fetch all items
    // async fetch({response}){
    //     const items = await Item.all();
    //     return response.ok(items)

    //     //retrive query params for category and search phrase
    //     //use values obtained to search on categories and wildcard on caption
    // }

    async fetch({request, response}){
        //retrive query params for category and search phrase
        //use values obtained to search on categories and wildcard on caption
        try{
        var phrase = request.input('phrase')
        var category_id = request.input('category')

        if(phrase){
            phrase = "%" + phrase + "%"
        }else{
            phrase = "%"
        }

        if(category_id && category_id != 'all'){
            const result = await Database
            .raw('SELECT items.* FROM worksample.items join item_categories ON items.id = item_categories.item_Id where item_categories.category_id = ? AND items.caption LIKE ?', [category_id, phrase])
            console.log(result)
            return response.ok(result[0])
        }else{
            const result = await Database
            .raw('SELECT items.* FROM worksample.items join item_categories ON items.id = item_categories.item_Id where items.caption LIKE ?', [phrase])
            return response.ok(result[0])
        }
        }catch(e){
            console.log(e)
        }
    }

    // async fetch2({response}){
    //     //mock
    //     await axios('http://127.0.0.1:3333/items')
    //         .then(response => {
    //             return response;
    //         });
    // }

    //fetch item by id
    async details({response, params}){
        //var id = params.id
        //var item = {"id":id, "name":`item ${id}`}
        var item = await Item.find(params.id)
        if(item){
            item.images = await item.itemImages().fetch()
            item.product_infomation = await item.productInformation().fetch()
            //item.comments = item.comments().fetch()
        }
        return response.ok(item)
    }

    //Strore Item
    async store({request, response}){  
        
        //Validate input
        const validation = await validate(request.all(), {
            caption: 'required|min:3|max:255',
            description: 'required|min:3'
        })

        if(validation.fails()){
            return response.badRequest(validation.messages);
            //return validation.messages()
        }
        
        const { caption, description, price, img, qty_in_store} = request.all()
        const item = new Item()
        item.caption = caption
        item.description = description
        item.price = price
        item.img = img
        item.qty_in_store = qty_in_store
        await item.save()
        return response.ok(item)
    }

    //fetch product information for a specific item by item id
    async fetchProductInformation({response, params}){
        const productInformation = await Database
        .table('product_informations')
        .where({'item_id':params.itemId})
        return response.ok(productInformation)
    }

    //Strore product information for item specified by item id
    async storeProductInformation({request, response, params}){        
        const {caption, value} = request.all()
        const productInformation = new ProductInformation()
        productInformation.item_id = params.itemId;
        productInformation.caption = caption;
        productInformation.value = value;
        await productInformation.save()
        return response.ok(productInformation)
    }

    //delete product information specified by item id and product information id
    async deleteProductInformation({response, params}){
        const count =await Database
            .table('product_informations')
            .where({'item_id':params.itemId})
            .where({'id':params.infoId})
            .del()
        return response.ok(count)
    }

    //fetch similar items to the item with item id provided
    async fetchSimilarItems({response, params}){
        const similarItems = await Database
        .table('item')
        .where({'item_id':params.itemId})
        return response.ok(similarItems)
    }

    //fetch comments made on item specified by item id
    async fetchComments({response, params}){
        const comments = await Database
        .table('comments')
        .where({'item_id':params.itemId})
        return response.ok(comments)
    }

    //fetch single comment by item id and comment id
    async fetchCommentSummary({response, params}){
        // const comments = await Database
        // .table('comments')
        // .where({'item_id':params.itemId})
        // .where({'id':params.commentId})
        // .first()
        // return response.ok(comments)
    }

    //fetch comment summary by item id
    //perform bulk operations on comment records to generate summary
    async fetchCommentDetail({response, params}){
        const comments = await Database
        .table('comment')
        .where({'item_id':params.itemId})
        .where({'id':params.commentId})
        .first()
        return response.ok(comments)
    }

    //Store comment on item
    async storeComment({request, response, params}){
        const {posted_by, note, rating} = request.all()
        
        const comment = new Comment()
        comment.posted_by = posted_by
        comment.note = note
        comment.rating = rating
        comment.item_id = params.itemId
        await comment.save()
        return response.ok(comment)
    }

    //delete comment on item by item id and comment id
    async deleteComment({response, params}){
        const count =await Database
            .table('comments')
            .where({'item_id':params.itemId})
            .where({'id':params.commentId})
            .del()
        return response.ok(count)
    }
}

module.exports = ItemController
