'use strict'

const ItemCategory = use('App/Models/ItemCategory')
const Database = use('Database')

class ItemCategoryController {

    async index({response, params}){
        const itemCategories = await Database
        .table('item_categories')
        .where({'category_id':params.categoryId})
        return response.ok(itemCategories)
    }

    async store({request, response, params}){
        const {item_id} = request.all()
        const itemCategory = new ItemCategory()
        itemCategory.category_id = params.categoryId
        itemCategory.item_id = item_id
        await itemCategory.save()
        return response.ok(itemCategory)
    }

    async delete({response, params}){
        const count = await Database
            .table('item_categories')
            .where({'category_id':params.categoryId})
            .where({'item_id':params.itemId})
            .del()
        return response.ok(count)
    }
}

module.exports = ItemCategoryController
