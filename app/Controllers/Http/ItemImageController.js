'use strict'

const ItemImage = use('App/Models/ItemImage')
const Database = use('Database')

class ItemImageController {

    async index({response, params}){
        const itemImages = await Database
        .table('item_images')
        .where({'item_id':params.itemId})
        return response.ok(itemImages)
    }

    async details({response, params}){
        const itemImage = await Database
        .table('item_images')
        .where({'item_id':params.itemId})
        .where({'id':params.imageId})
        .first()
        return response.ok(itemImage)
    }

    async store({request, response, params}){
        const {url, thumbnail_url, caption} = request.all()
        const itemImage = new ItemImage()
        itemImage.item_id = params.itemId
        itemImage.url = url
        itemImage.thumbnail_url = thumbnail_url
        itemImage.caption = caption
        await itemImage.save()
        return response.ok(itemImage)
    }

    async update({request, response, params}){ 
        const {url, thumbnail_url, caption} = request.all()

        const count = await Database
            .table('item_images')
            .where({'item_id':params.itemId})
            .where({'id':params.imageId})
            .update({"url":url, "thumbnail_url":thumbnail_url, "caption":caption})
        return response.ok(count)
    }

    async delete({response, params}){
        const count =await Database
            .table('item_images')
            .where({'item_id':params.itemId})
            .where({'id':params.imageId})
            .del()
        return response.ok(count)
    }
}

module.exports = ItemImageController
