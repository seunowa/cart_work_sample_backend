'use strict'

const Store = require("@adonisjs/framework/src/Route/Store")

const User = use('App/Models/User')

class RegisterController {
    async store({request, response}){  
        
        //Validate input
        const validation = await validate(request.all(), {
            email: 'required|email|unique:users',
            password: 'required|min:6'
        })

        if(validation.fails()){
            return response.badRequest(validation.messages);
            //return validation.messages()
        }

        const { email, password} = request.all()
        const user = await User.create({
            username: email,
            email : email,
            password : password
        })

        return response.ok()
    }
}

module.exports = RegisterController
