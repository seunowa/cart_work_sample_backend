'use strict'

const Item = use('App/Models/Item')
const CartItem = use('App/Models/CartItem')
const Database = use('Database')

class CartController {

    async index({response, params, auth}){

        const {username} = await auth.getUser()
        // const cartItems = await Database
        // .table('cart_items')
        // .where({'username':username})

        const cartItems = await CartItem
        .query()
        .where('username', username)
        .fetch()

        const promises = cartItems.rows.map(async (item)=>{ item.item = await Item.find(item.item_id)})
        await Promise.all(promises);

        return response.ok(cartItems)
    }

    async store({request, response, params, auth}){

        const {username} = await auth.getUser()
        const {item_id, quantity} = request.all()
        
        const cartItem = await Database
            .table('cart_items')
            .where({'username':username})
            .where({'item_id':item_id})
            .first()

        if(cartItem){            
            await Database
                .table('cart_items')
                .where({'username':username})
                .where({'item_id':item_id})
                .update({"quantity":quantity})

            cartItem.itemId = item_id
            cartItem.quantity = quantity

            return response.ok(cartItem)
        }else{
            const newCartItem = new CartItem()
            newCartItem.username = username
            newCartItem.item_id = item_id
            newCartItem.quantity = quantity
            
            await newCartItem.save()
            return response.ok(newCartItem)
        }
    }

    async delete({response, params, auth}){

        const {username} = await auth.getUser()
        const count = await Database
            .table('cart_items')
            .where({'username':username})
            .where({'item_id':params.itemId})
            .del()
        return response.ok(count)
    }
}

module.exports = CartController
