'use strict'

const OrderItem = use('App/Models/OrderItem')
const Order = use('App/Models/Order')
const Database = use('Database')
const Item = use('App/Models/Item')
const CartItem = use('App/Models/CartItem')

//setup twilio sms service
const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const client = require('twilio')(accountSid, authToken);

//setup twilio sendgrid email service
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_API_KEY) 
const Handlebars = require("handlebars")
const fs = require('fs')
const path = require('path');


class OrderController {

    async index({response, params, auth}){

        const {username} = await auth.getUser()
        const orders = await Database
        .table('orders')
        .where({'username':username})

        const promises = orders.map(async (order)=>{ 
            //fetch order items           
            order.order_items = await OrderItem.query()
                .where('order_id', order.id)
                .fetch()
            
            //fetch items
            const p =  order.order_items.rows.map(async (order_item)=>{
                order_item.item = await Item.find(order_item.item_id)
            })
            await Promise.all(p)
        })
        await Promise.all(promises)

        return response.ok(orders)
    }

    async details({response, params, auth}){

        const {username} = await auth.getUser()
        const orders = await Database
        .table('orders')
        .where({'username':username})
        .where({'id':params.orderId})
        return response.ok(orders)
    }

    async store({request, response, params, auth}){
        const {username, email, phone} = await auth.getUser()

        const {items} = request.all()
        
        const order = new Order()
        order.username = username
        await order.save()

        var orderItems = []
        var total = 0

        for (const i of items) {
            try{
                //fetch cart items in order
                const cartItem = await Database
                    .table('cart_items')
                    .where({'id':i.cart_item_id})
                    .where({'username':username})
                    .first()
                if(cartItem){
                    //fetch item in cart by item id
                    const item = await Item.find(cartItem.item_id)
                    if(item){    
                        //compute values for summar
                        var subtotal = item.price * cartItem.quantity
                        orderItems.push(
                            {
                                caption:item.caption,
                                price:item.price,
                                quantity:cartItem.quantity,
                                subtotal:subtotal
                            })
                        total += subtotal
                        
                        //set values for new order item to be saved
                        const orderItem = new OrderItem()
                        orderItem.order_id = order.id
                        orderItem.item_id = item.id
                        orderItem.quantity = cartItem.quantity
                        orderItem.price = item.price //set price from item retrived from db as this should not be obtained from the front end
                        await orderItem.save()
                    }

                    //remove item from cart
                    await Database
                    .table('cart_items')
                    .where({'id':i.cart_item_id})
                    .where({'username':username})
                    .del()
                }
            }catch(ex){
                console.log(ex)
            }
        }

        try{

        //send sms notification
        const smsMessage = {to:phone,
            body:`Dear ${username}, you placed order #${order.id}`}

        this.sendSms(smsMessage)

        //send email notification
        // console.log("********************************")
        // console.log(orderItems)
        // //const emailBody = EmailHelper.renderEmail(order)
        // const emailMessage = {to:email,
        //     subject:'Worksample: New Order',
        //     text:`Dear ${username}, you placed order #${order.id}`,
        //     html:``}

        // this.sendEmail(emailMessage)
        
        //send html formatted email notification
        const emailHtmlMessage = {
            to:email,
            subject:'Worksample: New Order',
            order:order,
            items:orderItems,
            total:total,
            html:``}
        //console.log(emailHtmlMessage) 
        this.sendHtmlEmail(emailHtmlMessage)
        }catch(e){
            console.log(e)
        }
        
        return response.ok(order)
    }

    async testSms({response}){
        //console.log("sending sms")
        console.log("sending sms")
        client.messages
        .create({
            body: 'This is the ship that made the Kessel Run in fourteen parsecs?',
            from: '+12015848993',
            to: '+2347038948610'
        })
        .then(message => {console.log(message.sid)
            console.log('Sms sent')});
        return response.ok({msg:'cool'})
    }

    sendSms(message){        
        client.messages
        .create({
            body:message.body,
            from:process.env.TWILIO_PHONE_NO,
            to:message.to
        })
        .then(message => console.log(message.sid))
    }

    async testEmail({response}){
        console.log("sending email")
        const msg = {
            to: 'seunowa@yahoo.com', // Change to your recipient
            from: 'seunowa@gmail.com', // Change to your verified sender
            subject: 'Sending with SendGrid is Fun',
            text: 'and easy to do anywhere, even with Node.js',
            html: '<strong>and easy to do anywhere, even with Node.js</strong>',
          }
          
          sgMail
            .send(msg)
            .then(() => {
              console.log('Email sent')
            })
            .catch((error) => {
              console.error(error)
            })
        return response.ok({msg:'cool'})
    }

    sendEmail(message){
        const from = process.env.SENDGRID_EMAIL;//get sender from env
        console.log(from)
        const msg = {
            to:message.to, // Change to your recipient
            //to:'seunowa@yahoo.com',
            //from: process.env.SENDGRID_EMAIL, // Change to your verified sender
            from:from,
            subject:message.subject,
            text:message.text,
            html:`<strong>${message.text}</strong>`,
          }
          
        sgMail
            .send(msg)
            .then(() => {
                console.log('Email sent')
            })
            .catch((error) => {
                console.error(error)
            })
    }

    sendHtmlEmail(message){

        const templateParam = {
            username:message.order.username,
            orderId:message.order.id,
            items:message.items,
            total:message.total
        }

        //const templateStr = fs.readFileSync(path.resolve(__dirname, 'email.hbs')).toString('utf8')
        const templateStr = fs.readFileSync(path.resolve(__dirname, 'email.hbs')).toString('utf8')
        const template = Handlebars.compile(templateStr, { noEscape: true })    
        const result = template(templateParam)

        //console.log(result)

        const from = process.env.SENDGRID_EMAIL;//get sender from env
        console.log(from)
        const msg = {
            to:message.to, // Change to your recipient
            //to:'seunowa@yahoo.com',
            //from: process.env.SENDGRID_EMAIL, // Change to your verified sender
            from:from,
            subject:message.subject,
            text:result,
            html:result,
          }
          
        sgMail
            .send(msg)
            .then(() => {
                console.log('Email sent')
            })
            .catch((error) => {
                console.error(error)
            })
    }

}

module.exports = OrderController
