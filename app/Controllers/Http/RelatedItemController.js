'use strict'
const Item = use('App/Models/Item')
const RelatedProduct = use('App/Models/RelatedProduct')
const Database = use('Database')

class RelatedItemController {

    async fetch({response, params}){
        const relItems = await Database
            //.select('id', 'caption', 'description', 'price').from("items")
            .table('related_products')
            .innerJoin('items', 'related_products.related_item_id', 'items.id')
            .where({'related_products.item_id':params.itemId})
        return response.ok(relItems)
        // const items = await Item.all();
        // return response.ok(items)
    }

    async store({request, response, params}){
        const {related_item_id} = request.all()
        const relatedProduct = new RelatedProduct()
        relatedProduct.item_id = params.itemId;
        relatedProduct.related_item_id = related_item_id
        await relatedProduct.save()
        return response.ok(relatedProduct)
    }

    async delete({response, params}){
        const count =await Database
            .table('related_products')
            .where({'item_id':params.itemId})
            .where({'related_item_id':params.relItemId})
            .del()
        return response.ok(count)
    }
}

module.exports = RelatedItemController
