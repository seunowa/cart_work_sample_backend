'use strict'

const View = use('App/Models/View')
const Database = use('Database')

class ViewHistoryController {

    async index({response, params}){
        const viewHistories = await Database
        .table('views')
        .where({'username':params.username})

        return response.ok(viewHistories)
    }

    async store({request, response, params}){

        const {item_id} = request.all()

        const viewHistory = await Database
            .table('views')
            .where({'username':params.username})
            .where({'item_id':item_id})
            .first()

        if(viewHistory){
            viewHistory.hits = 1 + viewHistory.hits

            const updatedHistoryView = await Database
                .table('views')
                .where({'username':params.username})
                .where({'item_id':item_id})
                .update({"hits": viewHistory.hits})
                return response.ok(viewHistory)
        }else{
            const newviewHistory = new View()
            newviewHistory.username = params.username
            newviewHistory.item_id = item_id
            newviewHistory.hits = 1
            
            await newviewHistory.save()
            return response.ok(newviewHistory)
        }

        
    }

    async delete({response, params}){
        const count = await Database
            .table('views')
            .where({'username':params.username})
            .where({'item_id':params.itemId})
            .del()
        return response.ok(count)
    }
}

module.exports = ViewHistoryController
