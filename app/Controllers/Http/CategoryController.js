'use strict'

const Category = use('App/Models/Category')
const Database = use('Database')

class CategoryController {

    async index({response}){
        const categories = await Database
        .table('categories')
        return response.ok(categories)
    }

    async details({response, params}){
        const category = await Database
        .table('categories')
        .where({'id':params.categoryId})
        .first()
        return response.ok(category)
    }

    async store({request, response}){
        const {caption, img} = request.all()
        console.log(caption)
        const category = new Category()
        category.caption = caption
        category.img = img
        console.log(category)
        await category.save()
        return response.ok(category)
    }

    async update({request, response, params}){ 
        const {caption, img} = request.all()

        const count = await Database
            .table('categories')
            .where({'id':params.categoryId})
            .update({"caption":caption, "img":img})
        return response.ok(count)
    }

    async delete({response, params}){
        const count =await Database
            .table('categories')
            .where({'id':params.categoryId})
            .where({'id':params.categoryId})
            .del()
        return response.ok(count)
    }
}

module.exports = CategoryController
