'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Item extends Model {
    itemImages () {
        return this.hasMany('App/Models/ItemImage', 'id', 'item_id')
      }
      
    productInformation () {
        return this.hasMany('App/Models/ProductInformation', 'id', 'item_id')
      }
      
    // comments () {
    //     return this.hasMany('App/Models/Comment', 'id', 'item_id')
    //   }
}

module.exports = Item
