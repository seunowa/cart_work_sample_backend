'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ItemCategory extends Model {
    item () {
        return this.hasOne('App/Models/Item')
      }
}

module.exports = ItemCategory
