'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CartItem extends Model {
    item() {
        // return this.belongsTo('App/Models/Item', 'item_id', 'id')
        return this.belongsTo('App/Models/Item')
    }
}

module.exports = CartItem
