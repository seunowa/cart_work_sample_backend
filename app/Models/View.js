'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class View extends Model {
    item () {
        return this.belongsTo('App/Models/Item', 'id', 'item_id')
      }
}

module.exports = View
