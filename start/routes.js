'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.on('/').render('home')
//Route.get('/items','ItemController.fetch')

Route.post('users', 'UserController.store')
Route.post('login', 'UserController.login')
Route.get('user', 'UserController.fetchUser').middleware('auth')

Route.group(() => {
    Route.get('/','ItemController.fetch')//filter by category and search phrase
    Route.post('/', 'ItemController.store')
    Route.get('/:id','ItemController.details')
    Route.put('/:id','ItemController.details')
    Route.delete('/:id','ItemController.details')

    Route.get('/:itemId/product_info/','ItemController.fetchProductInformation')
    Route.post('/:itemId/product_info', 'ItemController.storeProductInformation')
    Route.delete('/:itemId/product_info/:infoId','ItemController.deleteProductInformation')
    
    Route.get('/:itemId/related_items','RelatedItemController.fetch')
    Route.post('/:itemId/related_items', 'RelatedItemController.store')
    Route.delete('/:itemId/related_items/:relItemId','RelatedItemController.delete')
    
    Route.get('/:itemId/similar_items','ItemController.details')
    Route.post('/:itemId/similar_items', 'ItemController.store')
    Route.delete('/:itemId/similar_items/:similarItemId','ItemController.details')

    Route.get('/:itemId/comments/','ItemController.fetchComments')
    Route.get('/:itemId/comments/summary','ItemController.fetchCommentSummary')
    Route.post('/:itemId/comments', 'ItemController.storeComment')
    Route.get('/:itemId/comments/:commentId','ItemController.fetchCommentDetail')
    Route.delete('/:itemId/comments/:commentId','ItemController.deleteComment')

    Route.get('/:itemId/images','ItemImageController.index')
    Route.post('/:itemId/images', 'ItemImageController.store')
    Route.get('/:itemId/images/:imageId','ItemImageController.details')
    Route.put('/:itemId/images/:imageId','ItemImageController.update')
    Route.delete('/:itemId/images/:imageId','ItemImageController.delete')
}).prefix('/items')

Route.group(() => {
    Route.get('/','CategoryController.index')
    Route.post('/', 'CategoryController.store')
    Route.get('/:categoryId','CategoryController.details')
    Route.put('/:categoryId','CategoryController.details')
    Route.delete('/:categoryId','CategoryController.details')

    Route.get('/:categoryId/items' ,'ItemCategoryController.index')
    Route.post('/:categoryId/items' ,'ItemCategoryController.store')
    Route.delete('/:categoryId/items/:itemId' ,'ItemCategoryController.delete')
}).prefix('/categories')

Route.group(() => {
    Route.get('/','ViewHistoryController.index')
    Route.post('/', 'ViewHistoryController.store')
    Route.delete('/:itemId','ViewHistoryController.delete')
}).prefix('/users/:username/recently_viewed')
.middleware('auth')

Route.group(() => {
    Route.get('/','CartController.index')
    Route.post('/', 'CartController.store')
    Route.delete('/:itemId','CartController.delete')
}).prefix('/cart')
.middleware('auth')

Route.group(() => {
    Route.get('/','OrderController.index')
    Route.post('/', 'OrderController.store')
    Route.get('/test_sms','OrderController.testSms')
    Route.get('/test_email','OrderController.testEmail')
    Route.get('/:orderId','OrderController.details')
}).prefix('/orders')
.middleware('auth')

Route.group(() => {
    Route.get('/','ItemController.index')
    Route.post('/', 'ItemController.store')
    Route.get('/:itemId','ItemController.details')
    Route.put('/:itemId','ItemController.details')
    Route.delete('/:itemId','ItemController.details')
}).prefix('/users/:username/recommendations')
